import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by epakarb on 2017-01-05.
 */
public class VowelsTest {

    @Test
    public void verifyVowels() throws IOException {
        Vowels v = new Vowels();
        v.vowelsCheck();
        v.generateOutput();
        File output = new File(getClass().getResource("OUTPUT.txt").getPath().replaceFirst("/", ""));
        File expected = new File("src/test/java/resources/OUTPUT.txt");
        assertEquals("The files differ!",
                FileUtils.readFileToString(output, "utf-8"),
                FileUtils.readFileToString(expected, "utf-8"));
    }
}
