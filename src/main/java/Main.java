/**
 * Created by epakarb on 2017-01-05.
 */
public class Main {
    public static void main(String args[]){
        Vowels vowels = new Vowels();
        vowels.vowelsCheck();
        vowels.generateOutput();
    }
}
