import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Patryk on 2017-01-05.
 */

public class Vowels {
    private Map<String, StatisticsForWord> statistics;
    private Map<String, Integer> wordType;
    private String[] spitedInput;
    private URL url;

    Vowels(){
        wordType = new TreeMap<>();
        url = getClass().getResource("INPUT.txt");
        statistics = new TreeMap<>();
        prepareInput();
    }

    private void prepareInput(){
        String input;
        try {
            input = readFile(url.getPath().replaceFirst("/", "")).toLowerCase();
            spitedInput = input.replaceAll("[^a-zA-Z ]", "").split("\\s+");
        }catch (IOException e){
            System.out.println("Something go very wrong...\nI can not find input file under:\n" +  url.getPath().replaceFirst("/", ""));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getVowels(String word){
        return word.replaceAll("(?i)[^aeiou]", "");
    }

    private int getVowelsCount(String word){
        String vowels = getVowels(word);
        return vowels.length();
    }

    private String getUniqueVowels(String word){
        String vowels = getVowels(word);
        return vowels.replaceAll("(.)\\1+", "$1");
    }

    private String alphabeticalStringSort(String word){
        char[] chars = word.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

    public void vowelsCheck(){
        int wordLength;
        int vowelCount;
        String uniqueVowels;
        for(String word : spitedInput){
            wordLength = word.length();
            vowelCount = getVowelsCount(word);
            uniqueVowels = getUniqueVowels(word);
            String sortedVowels = alphabeticalStringSort(uniqueVowels);

            double avrForWord = (vowelCount != 0) ? (wordLength / vowelCount) : 0;

            String keyToStatistic = sortedVowels + Integer.toString(wordLength);

            StatisticsForWord statisticsForWord = statistics.containsKey(keyToStatistic) ? statistics.get(keyToStatistic) : new StatisticsForWord();

            statisticsForWord.incrementWord();
            statisticsForWord.addToSumVowels(avrForWord);

            statistics.put(keyToStatistic, statisticsForWord);
            wordType.put(keyToStatistic, vowelCount);
        }
    }

    public void generateOutput(){
        URL url = getClass().getResource("OUTPUT.txt");
        try(PrintWriter out = new PrintWriter(url.getPath().replaceFirst("/", ""))){
            StatisticsForWord statisticsForWord;
            for(String s : wordType.keySet()){
                statisticsForWord = statistics.get(s);
                double avarageVowels = statisticsForWord.getSumAvagareVowels() / statisticsForWord.getWordNumber();
                out.println("(" + s + ") -> " + avarageVowels);
            }
        }catch (IOException e){
            System.out.println("Something go very wrong...\nI can not find input file under:\n" +  url.getPath().replaceFirst("/", ""));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded);
    }

}
