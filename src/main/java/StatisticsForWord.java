/**
 * Created by epakarb on 2017-01-05.
 */
public class StatisticsForWord {
    private double sumAvagareVowels;
    private int wordNumber;

    public void setSumAvagareVowels(double sumAvagareVowels) {
        this.sumAvagareVowels = sumAvagareVowels;
    }

    public void setWordNumber(int wordNumber) {
        this.wordNumber = wordNumber;
    }

    public double getSumAvagareVowels() {

        return sumAvagareVowels;
    }

    public int getWordNumber() {
        return wordNumber;
    }

    public void incrementWord(){
        wordNumber++;
    }

    public StatisticsForWord() {
        sumAvagareVowels = 0;
        wordNumber = 0;
    }

    public void addToSumVowels(double avr){
        sumAvagareVowels += avr;
    }
}
